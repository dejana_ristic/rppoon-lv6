﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_RPPOON3
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker MycareTaker = new CareTaker();
            ToDoItem toDo = new ToDoItem("Osobna", "predati papire!", new DateTime(2020, 5, 14));
            MycareTaker.AddState(toDo.StoreState());
            Console.WriteLine(toDo.ToString());
            toDo.ChangeTask = "podići osobnu!";
            MycareTaker.AddState(toDo.StoreState());
            toDo.RestoreState(MycareTaker.getState(1));
            Console.WriteLine(toDo.ToString());

        }
    }
}
