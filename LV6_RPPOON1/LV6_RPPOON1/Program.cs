﻿using System;

namespace LV6_RPPOON1
{
    class Program
    {
        static void Main(string[] args)
        {   
            Notebook MyNote = new Notebook();
            MyNote.AddNote(new Note("Popis za trgovinu", "Jaja, mlijeko, sir, brašno"));
            MyNote.AddNote(new Note("TO DO List", "1. predati RPPOON, 2. predati izvješće iz Signala"));
            MyNote.AddNote(new Note("TITLE", "Add note!"));

            Iterator iterator = (Iterator)MyNote.GetIterator();
            for (Note note = iterator.First(); iterator.IsDone == false; note = iterator.Next())
            {
                note.Show();
            }
      
        }
    }
}
