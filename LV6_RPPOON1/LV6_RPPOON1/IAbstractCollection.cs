﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON1
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
