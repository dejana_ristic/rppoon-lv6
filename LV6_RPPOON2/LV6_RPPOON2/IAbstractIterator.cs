﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_RPPOON2
{
    interface IAbstractIterator
    {
        Box First();
        Box Next();
        bool IsDone { get; }
       Box Current { get; }
    }
}
