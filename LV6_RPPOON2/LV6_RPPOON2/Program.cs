﻿using System;

namespace LV6_RPPOON2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box ChosenItem = new Box();

            ChosenItem.AddProduct(new Product("Topshop IDOL midi dress", 39.05));
            ChosenItem.AddProduct(new Product("Nike Running Fast Tight", 34.71));
            ChosenItem.AddProduct(new Product(" Calvin Klein logo t-sirt", 28.20));

            BoxIterator iterator = (BoxIterator)ChosenItem.GetIterator();
            for (Product product = iterator.First(); iterator.IsDone == false; iterator.Next())
            {
                Console.WriteLine(product.ToString());
            }
            Console.ReadKey();
        }
    }
}
